import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { HttpApiUtil } from '../util/http-api.util';
import { APIError } from '../hero/hero.error';

@Injectable()
export class AuthService {
  constructor(private readonly httpApiUtil: HttpApiUtil) {}

  async validateUser(username: string, password: string): Promise<boolean> {
    if (!username || !password) {
      return false;
    }

    try {
      const res = await this.httpApiUtil.post<string>('/auth', {
        name: username,
        password,
      });

      return res.status === HttpStatus.OK;
    } catch (error) {
      Logger.error(
        `Validate User(${username}:${password}) API error: ${error.message}`,
      );
      if (error.response?.status === HttpStatus.UNAUTHORIZED) {
        return false;
      }

      throw new APIError();
    }
  }
}
