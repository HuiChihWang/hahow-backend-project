import { HttpException, HttpStatus } from '@nestjs/common';

export class APIError extends HttpException {
  constructor() {
    super('Backend API Error', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

export class HeroNotFoundError extends HttpException {
  constructor(heroId: string) {
    super(`Hero with id ${heroId} not found`, HttpStatus.NOT_FOUND);
  }
}
