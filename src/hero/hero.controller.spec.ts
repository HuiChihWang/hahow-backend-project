import { Test, TestingModule } from '@nestjs/testing';
import { HeroController } from './hero.controller';
import { HeroService } from './hero.service';
import { Request } from 'express';
import { Hero } from './hero.entity';
import { UtilModule } from '../util/util.module';
import { ConfigModule } from '../config/config.module';
import { HeroModule } from './hero.module';

describe('HeroController', () => {
  let controller: HeroController;
  let heroService: HeroService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HeroModule, ConfigModule, UtilModule.forRoot(UtilModule)],
    }).compile();

    controller = module.get(HeroController);
    heroService = module.get(HeroService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllHero', () => {
    it('should return all heroes with profile when user is available', async () => {
      const heroes: Hero[] = [
        {
          id: '1',
          name: 'Daredevil',
          image: 'daredevil.jpg',
          profile: {
            str: 1,
            int: 2,
            agi: 3,
            luk: 4,
          },
        },
      ];
      const request = {} as Request;
      request['user'] = 'test';

      const getHeroesSpy = jest
        .spyOn(heroService, 'getAllHero')
        .mockResolvedValue(heroes);

      const response = await controller.getAllHero(request);
      expect(response).toEqual(heroes);
      expect(getHeroesSpy).toHaveBeenCalledTimes(1);
      expect(getHeroesSpy).toHaveBeenCalledWith({ fetchProfile: true });
    });

    it('should return all heroes without profile when user is not available', async () => {
      const heroes: Hero[] = [
        {
          id: '1',
          name: 'Daredevil',
          image: 'daredevil.jpg',
        },
      ];
      const request = {} as Request;

      const getHeroesSpy = jest
        .spyOn(heroService, 'getAllHero')
        .mockResolvedValue(heroes);

      const response = await controller.getAllHero(request);
      expect(response).toEqual(heroes);
      expect(getHeroesSpy).toHaveBeenCalledTimes(1);
      expect(getHeroesSpy).toHaveBeenCalledWith({ fetchProfile: false });
    });
  });

  describe('getHeroById', () => {
    it('should return hero with profile when user is available', async () => {
      const hero: Hero = {
        id: '1',
        name: 'Daredevil',
        image: 'daredevil.jpg',
        profile: {
          str: 1,
          int: 2,
          agi: 3,
          luk: 4,
        },
      };
      const request = {} as Request;
      request['user'] = 'test';

      const getHeroByIdSpy = jest
        .spyOn(heroService, 'getHeroById')
        .mockResolvedValue(hero);

      const response = await controller.getHeroById('1', request);
      expect(response).toEqual(hero);
      expect(getHeroByIdSpy).toHaveBeenCalledTimes(1);
      expect(getHeroByIdSpy).toHaveBeenCalledWith('1', { fetchProfile: true });
    });

    it('should return hero without profile when user is not available', async () => {
      const hero: Hero = {
        id: '1',
        name: 'Daredevil',
        image: 'daredevil.jpg',
      };
      const request = {} as Request;

      const getHeroByIdSpy = jest
        .spyOn(heroService, 'getHeroById')
        .mockResolvedValue(hero);

      const response = await controller.getHeroById('1', request);
      expect(response).toEqual(hero);
      expect(getHeroByIdSpy).toHaveBeenCalledTimes(1);
      expect(getHeroByIdSpy).toHaveBeenCalledWith('1', { fetchProfile: false });
    });
  });
});
