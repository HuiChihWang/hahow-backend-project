import { Test } from '@nestjs/testing';
import { HttpApiUtil } from './http-api.util';
import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { UTIL_MODULE_OPTIONS_TOKEN } from './util.module.interface';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('HttpApiUtil', () => {
  let util: HttpApiUtil;
  let mockedAxiosInstance: AxiosInstance;

  beforeEach(async () => {
    mockedAxiosInstance = {
      get: jest.fn(),
      post: jest.fn(),
    } as any as AxiosInstance;
    mockedAxios.create.mockReturnValue(mockedAxiosInstance);

    const module = await Test.createTestingModule({
      providers: [
        HttpApiUtil,
        {
          provide: UTIL_MODULE_OPTIONS_TOKEN,
          useValue: {
            httpApiUtilOptions: {
              baseUrl: 'http://localhost:3000',
              retries: 5,
              delay: 0,
            },
          },
        },
      ],
    }).compile();

    util = module.get(HttpApiUtil);
  });

  it('should be defined', () => {
    expect(util).toBeDefined();
  });

  const createMockedAxiosError = (response?: unknown): AxiosError => {
    const error = new Error('test') as AxiosError;
    error.response = response as AxiosResponse;
    return error;
  };

  describe('get', () => {
    it('should return data from get request', async () => {
      const testResponse = { data: 'test', status: 200 } as AxiosResponse;
      jest.spyOn(mockedAxiosInstance, 'get').mockResolvedValue(testResponse);
      const result = await util.get('/test');
      expect(result).toEqual(testResponse);
    });
    it('should retry and throw error when axios error is thrown', async () => {
      const testError = createMockedAxiosError({
        status: 500,
      });
      jest.spyOn(mockedAxiosInstance, 'get').mockRejectedValue(testError);
      await expect(util.get('/test')).rejects.toThrow();
    });
    it('should retry and throw error when response status is 200 and data.code is 1000', async () => {
      const testResponse = {
        data: { code: 1000, message: 'test' },
        status: 200,
      } as AxiosResponse;
      jest.spyOn(mockedAxiosInstance, 'get').mockResolvedValue(testResponse);
      await expect(util.get('/test')).rejects.toThrow();
    });
    it('should send response after retry success', async () => {
      const successResponse = { data: 'test', status: 200 } as AxiosResponse;
      const badOkResponse = {
        data: { code: 1000, message: 'test' },
        status: 200,
      };
      const failError = createMockedAxiosError({ status: 500 });

      jest
        .spyOn(mockedAxiosInstance, 'get')
        .mockRejectedValueOnce(failError)
        .mockResolvedValueOnce(badOkResponse)
        .mockResolvedValueOnce(successResponse);
      const result = await util.get('/test');
      expect(result).toEqual(successResponse);
    });
    it('should return response when error response status is less than 500', async () => {
      const testError = createMockedAxiosError({ data: 'test', status: 400 });
      jest.spyOn(mockedAxiosInstance, 'get').mockRejectedValue(testError);
      await expect(util.get('/test')).rejects.toThrowError();
    });
  });
});
