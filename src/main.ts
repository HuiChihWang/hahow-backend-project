import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication, Logger } from '@nestjs/common';
import { ConfigService } from './config/config.service';

function setUpSwaggerInApp(app: INestApplication) {
  const swaggerConfig = new DocumentBuilder()
    .setTitle('Hahow Backend API')
    .setDescription('the doc is for hahow api')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  const configService = app.get(ConfigService);
  const swaggerPath = configService.getSwaggerUIPath();
  SwaggerModule.setup(swaggerPath, app, document);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  setUpSwaggerInApp(app);

  const appPort = app.get(ConfigService).getAppPort();
  const swaggerPath = app.get(ConfigService).getSwaggerUIPath();
  await app.listen(appPort);

  Logger.log(`App is running on port ${appPort}`);
  Logger.log(`Swagger UI is accessible at ${swaggerPath}`);
}
bootstrap();
