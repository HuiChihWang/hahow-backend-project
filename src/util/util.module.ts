import { Global, Module } from '@nestjs/common';
import { HttpApiUtil } from './http-api.util';
import {
  UTIL_MODULE_OPTIONS_TOKEN,
  UtilModuleOptions,
} from './util.module.interface';
import { createConfigurableDynamicRootModule } from '@golevelup/nestjs-modules';

@Global()
@Module({
  providers: [HttpApiUtil],
  exports: [HttpApiUtil],
})
export class UtilModule extends createConfigurableDynamicRootModule<
  UtilModule,
  UtilModuleOptions
>(UTIL_MODULE_OPTIONS_TOKEN) {}
