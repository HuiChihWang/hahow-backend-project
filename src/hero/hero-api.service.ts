import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { Hero, HeroProfile } from './hero.entity';
import { HttpApiUtil } from '../util/http-api.util';
import { APIError } from './hero.error';
import { CACHE_MANAGER, Cache } from '@nestjs/cache-manager';

@Injectable()
export class HeroApiService {
  private static heroProfileCachePrefix = 'hero-profile';

  constructor(
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly httpService: HttpApiUtil,
  ) {}

  async getHeroes(): Promise<Hero[]> {
    try {
      const response = await this.httpService.get<Hero[]>('/heroes');
      return response.data;
    } catch (error) {
      throw new APIError();
    }
  }

  async getHeroById(id: string): Promise<Hero> {
    try {
      const response = await this.httpService.get<Hero>(`/heroes/${id}`);
      return response.data;
    } catch (error) {
      if (error.response?.status === HttpStatus.NOT_FOUND) {
        return null;
      }
      throw new APIError();
    }
  }

  async getHeroProfile(id: string): Promise<HeroProfile> {
    const cacheKey = `${HeroApiService.heroProfileCachePrefix}-${id}`;
    const cachedProfile = await this.cacheManager.get<HeroProfile>(cacheKey);

    if (cachedProfile) {
      Logger.log(`Profile for hero ${id} found in cache`);
      return cachedProfile;
    }

    try {
      const response = await this.httpService.get<HeroProfile>(
        `/heroes/${id}/profile`,
      );
      await this.cacheManager.set(cacheKey, response.data);
      return response.data;
    } catch (error) {
      if (error.response?.status === HttpStatus.NOT_FOUND) {
        return null;
      }
      throw new APIError();
    }
  }
}
