import { Test, TestingModule } from '@nestjs/testing';
import { HeroService } from './hero.service';

import { HeroApiService } from './hero-api.service';
import { Hero, HeroProfile } from './hero.entity';
import { APIError, HeroNotFoundError } from './hero.error';
import { UtilModule } from '../util/util.module';
import { ConfigModule } from '../config/config.module';
import { CacheModule } from '@nestjs/cache-manager';

describe('HeroService', () => {
  let service: HeroService;
  let heroApiService: HeroApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule,
        CacheModule.register(),
        UtilModule.forRoot(UtilModule),
      ],
      providers: [HeroService, HeroApiService],
    }).compile();

    service = module.get(HeroService);
    heroApiService = module.get(HeroApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllHero', () => {
    it('should return all heroes without profile when profile option is set to false', async () => {
      const heroes: Hero[] = [
        {
          id: '1',
          name: 'Daredevil',
          image: 'daredevil.jpg',
        },
        {
          id: '2',
          name: 'Thor',
          image: 'thor.jpg',
        },
      ];

      const getHeroesSpy = jest
        .spyOn(heroApiService, 'getHeroes')
        .mockResolvedValue(heroes);
      const getHeroProfileSpy = jest.spyOn(heroApiService, 'getHeroProfile');
      const result = await service.getAllHero({ fetchProfile: false });
      expect(result).toEqual(heroes);
      expect(getHeroesSpy).toHaveBeenCalledTimes(1);
      expect(getHeroProfileSpy).not.toHaveBeenCalled();
    });

    it('should return all heroes with profile when profile option is set to true', async () => {
      const heroes: { [key: string]: Hero } = {
        '1': {
          id: '1',
          name: 'Daredevil',
          image: 'daredevil.jpg',
          profile: {
            str: 1,
            int: 2,
            agi: 3,
            luk: 4,
          },
        },
        '2': {
          id: '2',
          name: 'Thor',
          image: 'thor.jpg',
          profile: {
            str: 5,
            int: 6,
            agi: 7,
            luk: 8,
          },
        },
      };
      const heroesArr = Object.values(heroes);

      const getHeroProfileSpy = jest
        .spyOn(heroApiService, 'getHeroProfile')
        .mockImplementation(async (heroId: string) => heroes[heroId].profile);

      const getHeroesSpy = jest
        .spyOn(heroApiService, 'getHeroes')
        .mockResolvedValue(heroesArr);

      const result = await service.getAllHero({ fetchProfile: true });

      expect(getHeroesSpy).toHaveBeenCalledTimes(1);
      expect(getHeroProfileSpy).toHaveBeenCalledTimes(heroesArr.length);
      expect(result).toEqual(heroesArr);
    });

    it('should throw error when hero api query fail', async () => {
      jest.spyOn(heroApiService, 'getHeroes').mockRejectedValue(new APIError());
      await expect(service.getAllHero({ fetchProfile: false })).rejects.toThrow(
        APIError,
      );
    });
  });

  describe('getHeroById', () => {
    it('should return hero by id when fetchOption is false', async () => {
      const hero: Hero = {
        id: '1',
        name: 'Daredevil',
        image: 'daredevil.jpg',
      };

      const getHeroByIdSpy = jest
        .spyOn(heroApiService, 'getHeroById')
        .mockResolvedValue(hero);
      const getHeroProfileSpy = jest.spyOn(heroApiService, 'getHeroProfile');
      const result = await service.getHeroById('1', { fetchProfile: false });
      expect(getHeroByIdSpy).toHaveBeenCalledWith('1');
      expect(getHeroProfileSpy).not.toHaveBeenCalled();
      expect(result).toEqual(hero);
    });
  });
  it('should return hero by id with profile when fetchOption is true', async () => {
    const hero: Hero = {
      id: '1',
      name: 'Daredevil',
      image: 'daredevil.jpg',
    };

    const heroProfile: HeroProfile = {
      str: 1,
      int: 2,
      agi: 3,
      luk: 4,
    };

    const getHeroByIdSpy = jest
      .spyOn(heroApiService, 'getHeroById')
      .mockResolvedValue(hero);
    const getHeroProfileSpy = jest
      .spyOn(heroApiService, 'getHeroProfile')
      .mockResolvedValue(heroProfile);

    const result = await service.getHeroById('1', { fetchProfile: true });

    expect(getHeroByIdSpy).toHaveBeenCalledWith('1');
    expect(getHeroByIdSpy).toHaveBeenCalledTimes(1);
    expect(getHeroProfileSpy).toHaveBeenCalledWith('1');
    expect(getHeroProfileSpy).toHaveBeenCalledTimes(1);
    expect(result).toEqual({
      ...hero,
      profile: heroProfile,
    });
  });
  it('should throw error when hero is not found', async () => {
    jest.spyOn(heroApiService, 'getHeroById').mockResolvedValue(null);
    await expect(
      service.getHeroById('1', { fetchProfile: false }),
    ).rejects.toThrow(HeroNotFoundError);
  });
  it('should throw error when hero api query fail', async () => {
    jest.spyOn(heroApiService, 'getHeroById').mockRejectedValue(new APIError());
    await expect(
      service.getHeroById('1', { fetchProfile: false }),
    ).rejects.toThrow(APIError);
  });
});
