import { Module } from '@nestjs/common';
import { HeroService } from './hero.service';
import { HeroController } from './hero.controller';
import { HeroApiService } from './hero-api.service';
import { AuthModule } from '../auth/auth.module';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [
    AuthModule,
    CacheModule.registerAsync({
      useFactory: async () => ({
        ttl: 300000,
      }),
    }),
  ],
  providers: [HeroService, HeroApiService],
  controllers: [HeroController],
})
export class HeroModule {}
