import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        AuthGuard,
        {
          provide: AuthService,
          useValue: {
            validateUser: jest.fn(),
          },
        },
      ],
    }).compile();

    guard = module.get(AuthGuard);
    authService = module.get(AuthService);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });

  describe('canActivate', () => {
    it('should override user to username when authentication success ', async () => {
      const mockedRequest = {
        headers: { name: 'test', password: 'password' },
      };

      const context = {
        switchToHttp: () => ({
          getRequest: () => mockedRequest,
        }),
      } as any;

      const validateUserSpy = jest
        .spyOn(authService, 'validateUser')
        .mockResolvedValue(true);

      const isActivated = await guard.canActivate(context);
      expect(isActivated).toBe(true);

      expect(validateUserSpy).toHaveBeenCalledTimes(1);
      expect(validateUserSpy).toHaveBeenCalledWith('test', 'password');

      const request = context.switchToHttp().getRequest();
      expect(request['user']).toBe('test');
    });

    it('should not override user to username when authentication failed ', async () => {
      const mockedRequest = {
        headers: { name: 'test', password: 'password' },
      };

      const context = {
        switchToHttp: () => ({
          getRequest: () => mockedRequest,
        }),
      } as any;

      const validateUserSpy = jest
        .spyOn(authService, 'validateUser')
        .mockResolvedValue(false);

      const isActivated = await guard.canActivate(context);
      expect(isActivated).toBe(true);

      expect(validateUserSpy).toHaveBeenCalledTimes(1);
      expect(validateUserSpy).toHaveBeenCalledWith('test', 'password');

      const request = context.switchToHttp().getRequest();
      expect(request['user']).toBeUndefined();
    });
  });
});
