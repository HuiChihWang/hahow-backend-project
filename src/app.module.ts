import { Module } from '@nestjs/common';
import { HeroModule } from './hero/hero.module';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { UtilModule } from './util/util.module';
import { ConfigService } from './config/config.service';

@Module({
  imports: [
    HeroModule,
    ConfigModule,
    AuthModule,
    UtilModule.forRootAsync(UtilModule, {
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        httpApiUtilOptions: {
          baseUrl: configService.getHaHowBackendUrl(),
          timeout: 1000,
          retries: 5,
          delay: 300,
        },
      }),
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
