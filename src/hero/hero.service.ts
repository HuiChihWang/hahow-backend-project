import { Injectable } from '@nestjs/common';
import { Hero } from './hero.entity';
import { HeroApiService } from './hero-api.service';
import { HeroNotFoundError } from './hero.error';

interface GetHeroOption {
  fetchProfile?: boolean;
}

@Injectable()
export class HeroService {
  constructor(private readonly heroApiService: HeroApiService) {}

  async getAllHero(option: GetHeroOption): Promise<Hero[]> {
    const heroes = await this.heroApiService.getHeroes();

    if (!option.fetchProfile) {
      return heroes;
    }

    const getHeroProfilesPromises = heroes.map((hero) =>
      this.heroApiService.getHeroProfile(hero.id),
    );

    const heroProfiles = await Promise.all(getHeroProfilesPromises);
    return heroes.map((hero, index) => ({
      ...hero,
      profile: heroProfiles[index],
    }));
  }

  async getHeroById(id: string, option: GetHeroOption): Promise<Hero> {
    const hero = await this.heroApiService.getHeroById(id);

    if (!hero) {
      throw new HeroNotFoundError(id);
    }

    if (!option.fetchProfile) {
      return hero;
    }

    const profile = await this.heroApiService.getHeroProfile(id);
    return {
      ...hero,
      profile,
    };
  }
}
