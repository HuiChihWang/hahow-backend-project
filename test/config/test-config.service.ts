import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../src/config/config.service';

@Injectable()
export class TestConfigService extends ConfigService {
  getHaHowBackendUrl(): string {
    return 'https://hahow-recruit.herokuapp.com';
  }
}
