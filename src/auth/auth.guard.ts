import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const name = request.headers['name'] as string;
    const password = request.headers['password'] as string;
    const isAuthValid = await this.authService.validateUser(name, password);

    Logger.log(
      `Name: ${name}, Password: ${password} isAuthValid: ${isAuthValid}`,
    );

    if (isAuthValid) {
      request['user'] = name;
    }

    return true;
  }
}
