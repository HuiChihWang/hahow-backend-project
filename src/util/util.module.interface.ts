export interface HttpApiUtilOptions {
  baseUrl: string;
  timeout?: number;
  retries?: number;
  delay?: number;
}

export interface UtilModuleOptions {
  httpApiUtilOptions: HttpApiUtilOptions;
}

export const UTIL_MODULE_OPTIONS_TOKEN = 'UTIL_MODULE_OPTIONS_TOKEN';
