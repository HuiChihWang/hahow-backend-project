import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from './config.service';
import {
  ConfigModule as NestConfigModule,
  ConfigService as NestConfigService,
} from '@nestjs/config';

describe('ConfigService', () => {
  let service: ConfigService;
  let nestConfigService: NestConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [NestConfigModule],
      providers: [ConfigService],
    }).compile();

    service = module.get(ConfigService);
    nestConfigService = module.get(NestConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getHaHowBackendUrl', () => {
    it('should return the value of HAHOW_BACKEND_URL when HAHOW_BACKEND_URL is available from env', () => {
      const expectedUrl = 'http://localhost:3000';
      const getSpy = jest
        .spyOn(nestConfigService, 'get')
        .mockReturnValue(expectedUrl);

      expect(service.getHaHowBackendUrl()).toBe(expectedUrl);
      expect(getSpy).toHaveBeenCalledWith('HAHOW_BACKEND_URL');
    });
  });
});
