import { Injectable } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';
@Injectable()
export class ConfigService {
  constructor(private readonly nestConfigService: NestConfigService) {}

  getAppPort(): number {
    return this.nestConfigService.get<number>('APP_PORT') || 3000;
  }

  getSwaggerUIPath(): string {
    return this.nestConfigService.get<string>('SWAGGER_UI_PATH');
  }

  getHaHowBackendUrl(): string {
    return this.nestConfigService.get<string>('HAHOW_BACKEND_URL');
  }
}
