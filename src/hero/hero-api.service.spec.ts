import { Test, TestingModule } from '@nestjs/testing';
import { AxiosError, AxiosHeaders, AxiosResponse } from 'axios';
import { HeroApiService } from './hero-api.service';
import { HttpStatus } from '@nestjs/common';
import { APIError } from './hero.error';
import { UtilModule } from '../util/util.module';
import { HttpApiUtil } from '../util/http-api.util';
import { ConfigModule } from '../config/config.module';
import { CacheModule, Cache } from '@nestjs/cache-manager';

describe('HeroApiService', () => {
  let service: HeroApiService;
  let mockedHttpApiUtil: HttpApiUtil;
  let mockedCacheManager: Cache;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        UtilModule.forRoot(UtilModule),
        ConfigModule,
        CacheModule.register(),
      ],
      providers: [HeroApiService],
    }).compile();

    service = module.get(HeroApiService);
    mockedHttpApiUtil = module.get(HttpApiUtil);
    mockedCacheManager = module.get(Cache);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  const createMockedAxiosResponse = <T = unknown>(
    data: T = null,
    status: HttpStatus = HttpStatus.OK,
  ): AxiosResponse<T> => ({
    data,
    status: status,
    statusText: 'OK',
    headers: {},
    config: {
      headers: new AxiosHeaders(),
    },
  });

  describe('getHeroes', () => {
    it('should return all heroes without profile when profile option is set to false', async () => {
      const heroes = [
        {
          id: '1',
          name: 'Daredevil',
        },
        {
          id: '2',
          name: 'Thor',
        },
      ];

      const mockedAxiosResponse =
        createMockedAxiosResponse<typeof heroes>(heroes);

      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockResolvedValue(mockedAxiosResponse);
      const result = await service.getHeroes();
      expect(result).toEqual(heroes);
    });

    it('should throw error when api call fail', async () => {
      const mockedAxiosResponse = createMockedAxiosResponse(null);
      mockedAxiosResponse.status = HttpStatus.INTERNAL_SERVER_ERROR;

      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockRejectedValue(new Error('Internal Server Error'));
      await expect(service.getHeroes()).rejects.toThrow(APIError);
    });
  });

  describe('getHeroById', () => {
    it('should return hero by id', async () => {
      const hero = {
        id: '1',
        name: 'Daredevil',
      };

      const mockedAxiosResponse = createMockedAxiosResponse(hero);

      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockResolvedValue(mockedAxiosResponse);
      const result = await service.getHeroById('1');
      expect(result).toEqual(hero);
    });
    it('should return null when hero not found', async () => {
      const mockedAxiosError = new AxiosError();
      mockedAxiosError.response = {
        status: HttpStatus.NOT_FOUND,
      } as AxiosResponse;

      jest.spyOn(mockedHttpApiUtil, 'get').mockRejectedValue(mockedAxiosError);
      const result = await service.getHeroById('1');
      expect(result).toBeNull();
    });
    it('should throw error when api call fail', async () => {
      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockRejectedValue(new Error('Internal Server Error'));

      await expect(service.getHeroById('1')).rejects.toThrow(APIError);
    });
  });

  describe('getHeroProfile', () => {
    it('should return hero profile by id', async () => {
      const profile = {
        str: 1,
        int: 1,
        agi: 1,
        luk: 1,
      };

      const mockedAxiosResponse = createMockedAxiosResponse(profile);

      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockResolvedValue(mockedAxiosResponse);
      const result = await service.getHeroProfile('1');
      expect(result).toEqual(profile);
    });
    it('should set cache when profile is fetched', async () => {
      const profile = {
        str: 1,
        int: 1,
        agi: 1,
        luk: 1,
      };

      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockResolvedValue(createMockedAxiosResponse(profile));
      jest.spyOn(mockedCacheManager, 'get').mockResolvedValue(null);
      const setSpy = jest.spyOn(mockedCacheManager, 'set');

      const heroProfile = await service.getHeroProfile('1');
      expect(setSpy).toHaveBeenCalledWith('hero-profile-1', profile);
      expect(setSpy).toHaveBeenCalledTimes(1);
      expect(heroProfile).toBe(profile);
    });
    it('should return cached profile when profile is available', async () => {
      const profile = {
        str: 1,
        int: 1,
        agi: 1,
        luk: 1,
      };

      const fetchSpy = jest.spyOn(mockedHttpApiUtil, 'get');
      jest.spyOn(mockedCacheManager, 'get').mockResolvedValue(profile);

      const heroProfile = await service.getHeroProfile('1');
      expect(fetchSpy).not.toHaveBeenCalled();
      expect(heroProfile).toBe(profile);
    });
    it('should return null when hero profile not found', async () => {
      const mockedError = new AxiosError();
      mockedError.response = {
        status: HttpStatus.NOT_FOUND,
      } as AxiosResponse;

      jest.spyOn(mockedHttpApiUtil, 'get').mockRejectedValue(mockedError);
      const result = await service.getHeroProfile('1');
      expect(result).toBeNull();
    });
    it('should throw error when api call fail', async () => {
      jest
        .spyOn(mockedHttpApiUtil, 'get')
        .mockRejectedValue(new Error('Internal Server Error'));

      await expect(service.getHeroProfile('1')).rejects.toThrow(APIError);
    });
  });
});
