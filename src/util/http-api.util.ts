import { HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import {
  HttpApiUtilOptions,
  UTIL_MODULE_OPTIONS_TOKEN,
  UtilModuleOptions,
} from './util.module.interface';

@Injectable()
export class HttpApiUtil {
  private readonly axiosInstance: AxiosInstance;
  private readonly httpApiUtilOptions: HttpApiUtilOptions;

  constructor(
    @Inject(UTIL_MODULE_OPTIONS_TOKEN)
    private readonly utilOptions: UtilModuleOptions,
  ) {
    this.httpApiUtilOptions =
      utilOptions?.httpApiUtilOptions || ({} as HttpApiUtilOptions);

    this.axiosInstance = axios.create({
      baseURL: this.httpApiUtilOptions.baseUrl,
      timeout: this.httpApiUtilOptions.timeout || 3000,
    });
  }

  async get<T>(path: string) {
    const request = () => this.axiosInstance.get<T>(path);
    return this.retryRequest(
      request,
      this.httpApiUtilOptions.retries,
      this.httpApiUtilOptions.delay,
    );
  }

  async post<T>(path: string, body: unknown) {
    const request = () => this.axiosInstance.post<T>(path, body);
    return this.retryRequest(
      request,
      this.httpApiUtilOptions.retries,
      this.httpApiUtilOptions.delay,
    );
  }

  private async retryRequest<T>(
    request: () => Promise<AxiosResponse<T>>,
    retryTimes = 3,
    retryDelayMs = 500,
  ): Promise<AxiosResponse<T>> {
    const sleep = (ms: number) =>
      new Promise((resolve) => setTimeout(resolve, ms));

    let retryCount = 0;

    while (retryCount < retryTimes) {
      try {
        const res = await request();
        const data = res.data as any;

        if (res.status === HttpStatus.OK && data?.code === 1000) {
          throw new Error(
            `Error when calling api with http status 200: ${data?.message}`,
          );
        }

        return res;
      } catch (error) {
        if (error.response?.status < HttpStatus.INTERNAL_SERVER_ERROR) {
          throw error;
        }
      }

      ++retryCount;
      Logger.error(`Retry attempt #${retryCount}`);
      await sleep(retryDelayMs);
    }

    throw new Error(`Retry failed after ${retryTimes} attempts`);
  }
}
