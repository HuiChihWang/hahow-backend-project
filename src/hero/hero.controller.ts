import { Controller, Get, Param, Req, UseGuards } from '@nestjs/common';
import { HeroService } from './hero.service';
import { AuthGuard } from '../auth/auth.guard';
import { Request } from 'express';
import { ApiHeaders, ApiOperation, ApiParam } from '@nestjs/swagger';

@Controller('heroes')
@UseGuards(AuthGuard)
export class HeroController {
  constructor(private readonly heroService: HeroService) {}

  @ApiOperation({ summary: 'Get all heroes' })
  @ApiHeaders([
    {
      name: 'Name',
      description: 'Username',
    },
    {
      name: 'Password',
      description: 'Password',
    },
  ])
  @Get()
  async getAllHero(@Req() req: Request) {
    const name = req['user'] as string;
    return this.heroService.getAllHero({
      fetchProfile: !!name,
    });
  }

  @ApiOperation({ summary: 'Get hero by id' })
  @ApiHeaders([
    {
      name: 'Name',
      description: 'Username',
    },
    {
      name: 'Password',
      description: 'Password',
    },
  ])
  @ApiParam({
    name: 'heroId',
    description: 'query id for hero',
    type: 'string',
  })
  @Get(':heroId')
  async getHeroById(@Param('heroId') heroId: string, @Req() req: Request) {
    const name = req['user'] as string;
    return this.heroService.getHeroById(heroId, { fetchProfile: !!name });
  }
}
