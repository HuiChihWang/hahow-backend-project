import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ConfigService } from '../src/config/config.service';
import { TestConfigService } from './config/test-config.service';
import { Hero } from '../src/hero/hero.entity';

describe('HeroController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useClass(TestConfigService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('should be defined', () => {
    expect(app).toBeDefined();
  });

  describe('Get /heroes', () => {
    const heroesWithoutProfileResponse: Hero[] = [
      {
        id: '1',
        name: 'Daredevil',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/6/90/537ba6d49472b/standard_xlarge.jpg',
      },
      {
        id: '2',
        name: 'Thor',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/537bc7036ab02/standard_xlarge.jpg',
      },
      {
        id: '3',
        name: 'Iron Man',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg',
      },
      {
        id: '4',
        name: 'Hulk',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0/standard_xlarge.jpg',
      },
    ];
    const heroesWithProfileResponse: Hero[] = [
      {
        id: '1',
        name: 'Daredevil',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/6/90/537ba6d49472b/standard_xlarge.jpg',
        profile: {
          str: 2,
          int: 7,
          agi: 9,
          luk: 7,
        },
      },
      {
        id: '2',
        name: 'Thor',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/537bc7036ab02/standard_xlarge.jpg',
        profile: {
          str: 8,
          int: 2,
          agi: 5,
          luk: 9,
        },
      },
      {
        id: '3',
        name: 'Iron Man',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg',
        profile: {
          str: 6,
          int: 9,
          agi: 6,
          luk: 9,
        },
      },
      {
        id: '4',
        name: 'Hulk',
        image:
          'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0/standard_xlarge.jpg',
        profile: {
          str: 10,
          int: 1,
          agi: 4,
          luk: 2,
        },
      },
    ];

    it('get Heroes without authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes')
        .expect(200)
        .expect(heroesWithoutProfileResponse);
    });

    it('get Heroes with HeroProfile with correct authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes')
        .set('Name', 'hahow')
        .set('Password', 'rocks')
        .expect(200)
        .expect(heroesWithProfileResponse);
    });

    it('get Heroes with incorrect authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes')
        .set('Name', 'hahow')
        .set('Password', 'wrong')
        .expect(200)
        .expect(heroesWithoutProfileResponse);
    });
  });

  describe('Get /heroes/:heroId', () => {
    const heroWithoutProfileResponse: Hero = {
      id: '1',
      name: 'Daredevil',
      image:
        'http://i.annihil.us/u/prod/marvel/i/mg/6/90/537ba6d49472b/standard_xlarge.jpg',
    };

    const heroWithProfileResponse: Hero = {
      ...heroWithoutProfileResponse,
      profile: {
        str: 2,
        int: 7,
        agi: 9,
        luk: 7,
      },
    };

    it('get Hero without authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes/1')
        .expect(200)
        .expect(heroWithoutProfileResponse);
    });

    it('get Hero with HeroProfile with correct authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes/1')
        .set('Name', 'hahow')
        .set('Password', 'rocks')
        .expect(200)
        .expect(heroWithProfileResponse);
    });

    it('get Hero with incorrect authentication', async () => {
      return request(app.getHttpServer())
        .get('/heroes/1')
        .set('Name', 'hahow')
        .set('Password', 'wrong')
        .expect(200)
        .expect(heroWithoutProfileResponse);
    });
    it('get Hero with incorrect heroId', async () => {
      return request(app.getHttpServer())
        .get('/heroes/test')
        .set('Name', 'hahow')
        .set('Password', 'rocks')
        .expect(404);
    });
  });
});
