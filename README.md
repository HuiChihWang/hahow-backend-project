## Description
Hahow backend test, please refer to [Hahow Backend Test](https://github.com/hahow/hahow-recruit/blob/master/backend.md)

### Architecture
I use nestJS to build this project, and I use the following architecture to build this project.
![image info](hahow-BE-arch.drawio.svg?raw=true)


1. Under NestJS module, I create 3 different modules including HeroModule, AuthModule, and UtilModule
   - Hero Module: It is used to handle all hero related logic, including hero service, hero controller
   - Auth Module: It is used to handle all authentication related logic
   - Util Module: It is used to handle all common logic.Now there is only http api utility
2. Route setting
   - Because the auth is set at header and the private route and public route share same route, so I use `AuthGuard` to handle the auth logic.
   - Now auth will be checked first, if auth pass, use private route. On the other hand, use public route if auth fail.
3. Customized Axios Retry
   - Because data server is not stable, and there is some bad designed API response, I handle retry mechanism by myself.
   - `axios-retry` is hard to handle bad designed API response, i put all retry logic in `HttpApiUtil`.
   - retry will be executed once bad response received or other network problem occur.
4. Cache
   -  In real scenario, the profile of Hero is not changed frequently for the same player, so this could be cached for a while to reduce API queries.
   

## Tech Stacks
- NestJS
- Axios (3rd party lib I use)
- Cache-Manager (3rd party lib I use)

#### Commnet
1. Axios 
   - axios is used to send http request in NodeJS, and it is a promise based library, and it is easy to use. However, it is not a good design to use 3rd lib directly in our code, because it will make our code hard to maintain and test. 
   So I use `HttpApiUtil` to wrap axios, and I can easily mock it in my test.
2. Cache-Manager
    - Cache-Manager is used to handle simple logic, and it provide different interfaces for different store, like in-memory or Redis. I choose this for this scenario because we only want to do API cache. It,s easy to write simple cache logic.
    - However, if you use redis and want to use more complicated DS supported in Redis, i will not  recommend this because this only support simple put/get operations. In this case, you should use your own interface to do such operations. 
## Installation

```bash
$ pnpm install
```

## Running the app
set up environment variables in `./config/.env`


```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
````

now you can access app at http://localhost:3000
and you can access swagger at http://localhost:3000/api-doc, if you want change default port, please set `APP_PORT` in `./config/.env`

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

### Style
#### Comment Style
I prefer not to write comments in the code, because I think the code should be self-explanatory, and the comments will make the code harder to maintain. However, I will write comments if I think the code is hard to understand, or I have to explain the reason why I write the code in this way. Besides, some times there should be refactor or there is a bug, I will write comments to explain the reason why I write the code in this way (like TODO, FIXME...etc).
And in some cases, maybe I also write comments for interfaces to add more detailed docs of usage.

### Problem

#### There is no flexible retry mechanism supported in nestJS HttpModule, so I have to implement it by myself, and I have to use `axios-retry` to make it work.

Because when I am doing e2e tests, I found api call sometimes will fail (including timeout, or server error, even bad response), I have to use a more resilient design to make api call stable. However, the problem is that in NestJS, there is no config I can set to use original HttpModule because it uses `axios` to send request.
To solve this problem I have to use `axios-retry` to make my own http services. But I found `axios-retry` cannot handle bad response case unless you override interceptors. So I have to use `HttpApiUtil` to wrap `axios` and handle retry mechanism by myself.

In the beginning, I use HttpModule directly, so I need to spend a little effort to refactor my code to use `HttpApiUtil` to wrap `axios` and handle retry mechanism by myself.
I learned from this mistakes that sometimes we will directly use 3rd lib its-self without providing a interface for it, and it will make our code hard to maintain and test.

### Future Work
1. Dockerization
2. Redis Cache
