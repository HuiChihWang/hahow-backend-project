import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AxiosError, AxiosHeaders, AxiosResponse } from 'axios';
import { HttpStatus } from '@nestjs/common';
import { APIError } from '../hero/hero.error';
import { UtilModule } from '../util/util.module';
import { HttpApiUtil } from '../util/http-api.util';
import { ConfigModule } from '../config/config.module';

describe('AuthService', () => {
  let service: AuthService;
  let httpApiUtil: HttpApiUtil;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule, UtilModule.forRoot(UtilModule)],
      providers: [AuthService],
    }).compile();

    service = module.get(AuthService);
    httpApiUtil = module.get(HttpApiUtil);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  const createMockedAxiosResponse = <T = unknown>(
    data: T = null,
    status: HttpStatus,
  ): AxiosResponse<T> => ({
    data,
    status: status,
    statusText: 'OK',
    headers: {},
    config: {
      headers: new AxiosHeaders(),
    },
  });

  describe('validateUser', () => {
    it('should return true when authentication is successful ', async () => {
      const testUser = {
        username: 'test',
        password: 'test',
      };

      const mockedResponse = createMockedAxiosResponse(null, HttpStatus.OK);
      jest.spyOn(httpApiUtil, 'post').mockResolvedValue(mockedResponse);

      expect(
        await service.validateUser(testUser.username, testUser.password),
      ).toBe(true);
    });
  });
  it('should return false when authentication is failed ', async () => {
    const testUser = {
      username: 'test',
      password: 'test',
    };

    const mockedError = new AxiosError();
    mockedError.response = {
      status: HttpStatus.UNAUTHORIZED,
    } as AxiosResponse;

    jest.spyOn(httpApiUtil, 'post').mockRejectedValue(mockedError);
    expect(
      await service.validateUser(testUser.username, testUser.password),
    ).toBe(false);
  });
  it('should not call api when username or password is empty', async () => {
    const testAuths = {
      authOnlyName: {
        username: 'test',
      },
      authOnlyPassword: {
        password: 'test',
      },
      emptyAuth: {
        username: '',
        password: '',
      },
    };

    const postSpy = jest.spyOn(httpApiUtil, 'post');

    for (const testAuth in testAuths) {
      const user = testAuths[testAuth];
      const result = await service.validateUser(user.username, user.password);
      expect(result).toBe(false);
      expect(postSpy).not.toHaveBeenCalled();
    }
  });
  it('should throw error when API error occurred', async () => {
    const testUser = {
      username: 'test',
      password: 'test',
    };

    jest.spyOn(httpApiUtil, 'post').mockRejectedValue(new Error('API Error'));
    await expect(
      service.validateUser(testUser.username, testUser.password),
    ).rejects.toThrow(APIError);
  });
});
